// https://docs.cypress.io/api/introduction/api.html

describe("My First Test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "TODO LIST");
    cy.get("form");
  });
  it("add todo", () => {
    cy.visit("/");
    cy.get("input.input").type("new task");
    cy.get("button.button").trigger("click");
    cy.get("ol");
  });
});
