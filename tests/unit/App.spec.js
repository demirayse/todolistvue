import { mount } from "@vue/test-utils";
import App from "@/App.vue";
describe("App unit test:", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(App, {
      propsData: {
        items: {
          task: "run",
          done: false,
        },
      },
    });
    expect(wrapper.find("button").text()).toBe("ADD");
    //expect(wrapper.find("ol").contains("span")).toBe.true;
    expect(wrapper.find("form").exists()).toBe(true);
    expect(wrapper.find("h1").text()).toBe("TODO LIST");
  });
});
/*describe("App unit test:", () => {
  it("input reading", () => {
    const wrapper = mount(App, {
      data() {
        return {
          todo: "run",
        };
      },
    });
    expect((wrapper.find(".input").value = "run")).toBe("run");
  });
});*/
